# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 17:44:20 2017

Python 3

@author: Thomas Reesink
"""

from twython import Twython
from twitter_credentials import get_credentials
from datetime import datetime
import simplejson

api_key,api_secret = get_credentials()
pechtold_screen_name = 'APechtold'
wilders_screen_name = 'geertwilderspvv'

def get_access_token(app_key, app_secret):
    twitter = Twython(app_key, app_secret, oauth_version=2)
    access_token = twitter.obtain_access_token()
    return access_token

def mine_all_tweets_from_user(screen_name, api_key=api_key, api_secret=api_secret):
    twitter = Twython(api_key, api_secret)
    
    #initialize a list to hold all the Tweets
    all_tweets = list()
    print("downloading all tweets for %s" % screen_name)
    
    #make initial request for most recent tweets (200 is the maximum allowed count)
    new_tweets = twitter.get_user_timeline(screen_name = screen_name,count=200)
	
    #save most recent tweets
    all_tweets.extend(new_tweets)
    
    print("...%s tweets downloaded so far" % (len(all_tweets)))
    
    #save the id of the oldest tweet less one
    oldest = all_tweets[-1].get('id') - 1
    
    
    while len(new_tweets) > 0:
		
        #all subsiquent requests use the max_id param to prevent duplicates
        new_tweets = twitter.get_user_timeline(screen_name = screen_name,count=200,max_id=oldest)
		
        #save most recent tweets
        all_tweets.extend(new_tweets)
		
        #update the id of the oldest tweet less one
        oldest = all_tweets[-1].get('id') - 1
        
        print("...%s tweets downloaded so far" % (len(all_tweets)))
        
    return all_tweets

def mine_wilders_and_pechtold():
    now = datetime.now()
    pechtold_tweets=mine_all_tweets_from_user(pechtold_screen_name)
    
    with open('pechtold_twitter'+str(now.year)+str(now.month)+str(now.day)+str(now.hour)+str(now.minute)+'.json', 'w') as f :
        simplejson.dump(pechtold_tweets,f)
        
    wilders_tweets = mine_all_tweets_from_user(wilders_screen_name)
    with open('wilders_twitter'+str(now.year)+str(now.month)+str(now.day)+str(now.hour)+str(now.minute)+'.json', 'w') as f :
        simplejson.dump(wilders_tweets,f)
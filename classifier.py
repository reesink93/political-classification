# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 17:44:20 2017

Python 3

@author: Thomas Reesink
"""
import math
        
class BigramClassifier:
    
    def __init__(self):
        self.x = 'Hello'
    
    m_tokens=list()
    f_tokens=list()
    m_tokensets=list()
    f_tokensets=list()
    m_token_amount=dict()
    f_token_amount=dict()
    m_token_bi_amount=dict()
    f_token_bi_amount=dict()
    m_token_bi_propability=dict()
    f_token_bi_propability=dict()
    m_word_amount=0
    f_word_amount=0
    
    def set_zero(self):
        self.m_token_amount=dict()
        self.f_token_amount=dict()
        self.m_token_bi_amount=dict()
        self.f_token_bi_amount=dict()
        self.m_token_bi_propability=dict()
        self.f_token_bi_propability=dict()
        self.male_word_amount=0
        self.female_word_amount=0
    
    def update_statistics(self):
        self.set_zero()
        
        self.f_word_amount=len(self.f_tokens)
        
        self.m_word_amount=len(self.m_tokens)
        
        for token in self.m_tokens:
            amount = self.m_token_amount.get(token)
            if amount == None:
                amount = 0
            self.m_token_amount[token] = amount+1
            
        for token in self.f_tokens:
            amount = self.f_token_amount.get(token)
            if amount == None:
                amount = 0
            self.f_token_amount[token] = amount+1
        
        for tokenset in self.m_tokensets:
            for i in range(0,len(tokenset)-1):
                w = tokenset[i]
                ww = tokenset[i+1]
                
                amount = self.m_token_bi_amount.get((w,ww))
                if amount == None:
                    amount = 0
                self.m_token_bi_amount[(w,ww)] = amount+1
                
        for tokenset in self.f_tokensets:
            for i in range(0,len(tokenset)-1):
                w = tokenset[i]
                ww = tokenset[i+1]
                
                amount = self.f_token_bi_amount.get((w,ww))
                if amount == None:
                    amount = 0
                self.f_token_bi_amount[(w,ww)] = amount+1
        
        for w,ww in self.f_token_bi_amount:
            self.f_token_bi_propability[(w,ww)] = self.f_word_bigram_posibility(w,ww)
        
        for w,ww in self.m_token_bi_amount:
           self.m_token_bi_propability[(w,ww)] = self.m_word_bigram_posibility(w,ww)
    
    def train_single_m_no_update(self, new_tokens):
        tokenset_be=['BEGINNING_OF_TWEET']+new_tokens+['ENDING_OF_TWEET']
        self.m_tokens.extend(tokenset_be)
        self.m_tokensets.append(tokenset_be)
        
    def train_single_f_no_update(self, new_tokens):
        tokenset_be=['BEGINNING_OF_TWEET']+new_tokens+['ENDING_OF_TWEET']
        self.f_tokens.extend(tokenset_be)
        self.f_tokensets.append(tokenset_be)
            
    def train_m(self,set_of_tokensets):
        self.m_tokens=list()
        self.m_tokensets=list()
        
        for tokenset in set_of_tokensets:
            self.train_single_m_no_update(tokenset)
        
        
    def train_f(self,set_of_tokensets):
        self.f_tokens=list()
        self.f_tokensets=list()
        
        for tokenset in set_of_tokensets:
            self.train_single_f_no_update(tokenset)
            
    def train(self,m_set_of_tokensets, f_set_of_tokensets):
        self.train_m(m_set_of_tokensets)
        self.train_f(f_set_of_tokensets)
        self.update_statistics()
        
    def m_word_unigram_posibility(self,word):
        m_amount = self.m_token_amount.get(word,0.)
        
        m_posibility = (1.+m_amount)/(len(self.m_token_amount)+sum(self.m_token_amount.values()))
        return m_posibility
    
    def f_word_unigram_posibility(self, word):
        f_amount = self.f_token_amount.get(word,0.)

        f_posibility = (1.+f_amount)/(len(self.f_token_amount)+sum(self.f_token_amount.values()))
        return f_posibility
    
    def mf_word_unigram_posibility(self,word):
        return (self.m_word_posibility(word),self.f_word_posibility(word))

    def m_word_bigram_posibility(self,word1,word2):
        m_amount = self.m_token_bi_amount.get((word1,word2),0.)
        word1_amount = self.m_token_amount.get(word1,0.)
        
        
        return (m_amount+1.)/(word1_amount+len(self.m_token_amount))
    
    def f_word_bigram_posibility(self,word1,word2):
        f_amount = self.f_token_bi_amount.get((word1,word2),0.)
        word1_amount = self.f_token_amount.get(word1,0.)
        
        return (f_amount+1.)/(word1_amount+len(self.f_token_amount))
    
    def m_chance(self,tokenset):
        chance = 0.
        for i in range(0,len(tokenset)-1):
            w = tokenset[i]
            ww = tokenset[i+1]
            chance += math.log(self.m_word_bigram_posibility(w,ww),2)
        return chance
    
    def m_unigram_chance(self,tokenset):
        chance = 0.
        for word in tokenset:
            chance += math.log(self.m_word_unigram_posibility(word),2)
        return chance
            
    def f_chance(self,tokenset):
        chance = 0.
        for i in range(0,len(tokenset)-1):
            w = tokenset[i]
            ww = tokenset[i+1]
            chance += math.log(self.f_word_bigram_posibility(w,ww),2)
        return chance

    def f_unigram_chance(self,tokenset):
        chance = 0.
        for word in tokenset:
            chance += math.log(self.f_word_unigram_posibility(word),2)
        return chance
            

    def classify(self,tokenset):
        if self.m_chance(tokenset) > self.f_chance(tokenset):
            return 'm'
        else:
            return 'f'
        
    def unigram_classify(self,tokenset):
        if self.m_unigram_chance(tokenset) > self.f_unigram_chance(tokenset):
            return 'm'
        else:
            return 'f'
        

# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 17:44:20 2017

Python 3

@author: Thomas Reesink
"""
import simplejson as json
from nltk.tokenize import RegexpTokenizer
from classifier import BigramClassifier
import random
import re

pechtold_file = 'pechtold_twitter20171091849.json'
wilders_file = 'wilders_twitter20171091849.json'

pechtold = dict()
wilders = dict()


with open(pechtold_file, 'r') as f:
     pechtold = json.load(f)
with open(wilders_file, 'r') as f:
     wilders = json.load(f)
     
w_tweets = [tweet.get('text') for tweet in wilders if tweet.get('retweeted_status')==None] # remove retweets
p_tweets = [tweet.get('text') for tweet in pechtold if tweet.get('retweeted_status')==None]

w_tokensets = list()
p_tokensets = list()

tokenizer = RegexpTokenizer(r'\w+')

for tweet_text in w_tweets:
    tweet_text = re.sub(r'https?:\/\/.*[\r\n ]*', ' ', tweet_text, flags=re.MULTILINE)
    tweet_text = re.sub(r'@([A-Za-z]+[A-Za-z0-9_]+)', ' SCREEN_NAME ', tweet_text, flags=re.MULTILINE)
    new_tokens = [i.lower() for i in tokenizer.tokenize(tweet_text)]
    if len(new_tokens)>0:
        w_tokensets.append(new_tokens)
    
for tweet_text in p_tweets:
    tweet_text = re.sub(r'https?:\/\/.*[\r\n ]*', ' ', tweet_text, flags=re.MULTILINE)
    tweet_text = re.sub(r'@([A-Za-z]+[A-Za-z0-9_]+)', ' SCREEN_NAME ', tweet_text, flags=re.MULTILINE)
    new_tokens = [i.lower() for i in tokenizer.tokenize(tweet_text)]
    if len(new_tokens)>0:
        p_tokensets.append(new_tokens)    

random.shuffle(w_tokensets) 
random.shuffle(p_tokensets)     

#Equalize tokensets to be equal in size
if len(w_tokensets)>len(p_tokensets):
    w_tokensets = w_tokensets[0:len(p_tokensets)]
else:
    p_tokensets = p_tokensets[0:len(w_tokensets)]
    
# 80-20 split for test and train 
w_test = w_tokensets[0:int(len(w_tokensets)/5)]
w_train = w_tokensets[int(len(w_tokensets)/5):len(w_tokensets)]
p_test = p_tokensets[0:int(len(p_tokensets)/5)]
p_train = p_tokensets[int(len(p_tokensets)/5):len(p_tokensets)]
    
    
def shuffle_2_lists(list1,list2):
    combined = list(zip(list1, list2))
    random.shuffle(combined)
    list1[:], list2[:] = zip(*combined)
    
    return list1,list2
    
    
#split training set into 10 parts for 10 fold cross validation

train_w_sets= [list() for x in range(0,10)]
train_p_sets= [list() for x in range(0,10)]

valid_p_sets=[list() for x in range(0,10)]
valid_w_sets=[list() for x in range(0,10)]



for i in range(0,10):
    w_temp = w_train[int((len(w_train)/10)*i):int(len(w_train)/10*(i+1))]
    p_temp = p_train[int(len(p_train)/10*i):int(len(p_train)/10*(i+1))]
    valid_w_sets[i] += w_temp
    valid_p_sets[i] += p_temp    
    
for i in range (0,10):
    for j in range(0,10):
        if (i!=j):
            train_w_sets[i] += (valid_w_sets[j])
            train_p_sets[i] += (valid_p_sets[j])
    
    
    
classifiers = [BigramClassifier() for x in range(0,10)]
confusion_matrixes = [{'TP':0,'TW':0,'FP':0,'FW':0} for x in range(0,10)]
accuracies = [0. for x in range(0,10)]

for i in range(0,10):
    classifiers[i].train(m_set_of_tokensets=train_w_sets[i], f_set_of_tokensets=train_p_sets[i])

for i in range(0,10):
    classifier = classifiers[i]
    valid_w = valid_w_sets[i]
    valid_p = valid_p_sets[i]
    
    for tweet in valid_w:
        if classifier.classify(tweet)== 'm':
             confusion_matrixes[i]['TW']+=1
        else:
             confusion_matrixes[i]['FP']+=1
            
    for tweet in valid_p:
        if classifier.classify(tweet)== 'f':
             confusion_matrixes[i]['TP']+=1
        else:
             confusion_matrixes[i]['FW']+=1
    accuracies[i] = (confusion_matrixes[i]['TP']+confusion_matrixes[i]['TW'])/(len(valid_w)+len(valid_p))
    
best_fold = accuracies.index(max(accuracies))

classifier = classifiers[best_fold]

confusion_matrix = {'TP':0,'TW':0,'FP':0,'FW':0}

for tweet in w_test:
    if classifier.classify(tweet)== 'm':
        confusion_matrix['TW']+=1
    else:
        confusion_matrix['FP']+=1
        
for tweet in p_test:
    if classifier.classify(tweet)== 'f':
         confusion_matrix['TP']+=1
    else:
         confusion_matrix['FW']+=1

accuracy = (confusion_matrix['TP']+confusion_matrix['TW'])/(len(w_test)+len(p_test))
print(accuracy)
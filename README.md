# README #
The goal of this project is to be able to distinguish between two dutch politicians on twitter.
The reason twitter was chosen is because it has a very good API for research purposes.

Though we focus  on two politicians, the original goal was to be able to classify politicians by their party.
As a proof of concept we choose to distinguish between the most prominent user of a progressive party and the most prominent user of a conservative party, "Politieke Partij Democraten 66" (D66) and "Partij voor de Vrijheid" (PVV).
The most prominent member of D66 is Alexander Pechtold and the most prominent member of PVV is Geert Wilders.
These users both have more than 10,000 tweets under their official twitter-account which makes them more than qualified candidates for this project.

### How do I get set up? ###

Requirements:

 - Python 3
 - nltk
 - simplejson

In order to use twitter_retriever.py, twitter_credentials.py has to be modified

### Report/dataset ###
Allong with the project there was a report which can be found in the downloads section.

In the downloads section there is also the dataset used for the project(retrieved by the twitter_retriever.py

### Who do I talk to? ###

 - Thomas Reesink
 - Semere Bitew